#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <catch2/catch_test_macros.hpp>
#include "fixtures_block.hpp"

#include "packetformat/block_parser.hpp"

using namespace std;
using namespace felix::packetformat;


struct OperationsCounter : ParserOperations
{
  unsigned subchunks = 0;
  unsigned subchunks_with_error = 0;
  unsigned chunks = 0;
  unsigned chunks_with_error = 0;
  unsigned blocks = 0;
  unsigned blocks_with_error = 0;

  void subchunk_processed(const subchunk&)
  {
    subchunks++;
  }
  void subchunk_processed_with_error(const subchunk&)
  {
    subchunks_with_error++;
  }
  void chunk_processed(const chunk&)
  {
    chunks++;
  }
  void chunk_processed_with_error(const chunk&)
  {
    chunks_with_error++;
  }
  void shortchunk_processed(const shortchunk&)
  {
    chunks++;
  }
  void block_processed(const block&)
  {
    blocks++;
  }
  void block_processed_with_error(const block&)
  {
    blocks_with_error++;
  }
};


TEST_CASE( "Block Parser Initialization", "[parser]" )
{
  typedef BlockParser<ParserOperations> block_parser;
  block_parser parser(block_parser::Operations ops);
  REQUIRE( true );
}

TEST_CASE( "Parse Block Detail", "[parser]" )
{
  typedef detail::BlockParserDetail<OperationsCounter> block_parser;
  OperationsCounter ops;
  block_parser parser;

  const block* b = block_from_bytes(empty_block);

  parser.process(b, ops);
  REQUIRE( ops.blocks == 1 );
}

TEST_CASE( "Parse Block", "[parser]" )
{
  typedef BlockParser<OperationsCounter> block_parser;
  OperationsCounter ops;
  block_parser parser(ops);

  const block* b = block_from_bytes(empty_block);

  parser.process(b);
  REQUIRE( ops.blocks == 1 );
  REQUIRE( ops.subchunks == 510 /* null subchunks */ );
}

void
read_blocks(ifstream& in, vector<block>& blocks)
{
  char buf[BLOCKSIZE];
  while( in.read(buf, BLOCKSIZE) )
    {
      blocks.push_back(*block_from_bytes(buf));
    }
}

bool
read_file(std::string filename, vector<block>& blocks)
{
  {
    ifstream in(filename, ios::binary);
    if(!in)
      {
        cerr << "Could not open " << filename << " for reading\n";
        return false;
      }
    read_blocks(in, blocks);
  }

  return true;
}

TEST_CASE( "Parse subchunks", "[parser]" )
{
  typedef detail::BlockParserDetail<OperationsCounter> block_parser;
  OperationsCounter ops;
  block_parser parser;
  std::vector<block> blocks;
  REQUIRE( read_file("test/sample_blocks_100.bin", blocks) );

  detail::subchunk_stack_t subchunks;
  bool errors_occured = false;
  parser.read_subchunks(blocks.data(), subchunks,	ops, errors_occured);

  REQUIRE( errors_occured == false );
  REQUIRE( subchunks.size() > 0 );

}


TEST_CASE( "Parse 100 blocks", "[parser]" )
{
  typedef BlockParser<OperationsCounter> block_parser;
  OperationsCounter ops;
  block_parser parser(ops);
  std::vector<block> blocks;
  REQUIRE( read_file("test/sample_blocks_100.bin", blocks) );

  parser.process(blocks.data(), blocks.size());

  REQUIRE( ops.subchunks_with_error == 0 );
  REQUIRE( ops.chunks_with_error == 0 );
  REQUIRE( ops.blocks == 100 );
  REQUIRE( ops.blocks_with_error == 0 );
  REQUIRE( ops.chunks == 53 );
}
