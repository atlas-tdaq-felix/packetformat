#include <catch2/catch_test_macros.hpp>
#include "fixtures_block.hpp"

#include "packetformat/block_format.hpp"

#include <cstring>
#include <string>

using namespace felix::packetformat;


TEST_CASE( "Block Format Size", "[format]" )
{
  REQUIRE( sizeof(block) == BLOCKSIZE );
}


TEST_CASE( "Block Conversion", "[format]")
{
  const block* b = block_from_bytes(empty_block);
  REQUIRE( (b->sob == START_OF_BLOCK || b->sob == START_OF_BLOCK_32B) );
  REQUIRE( b->elink == 0 );
//  REQUIRE( b->gbt == 0 );
//  REQUIRE( b->egroup == 0 );
//  REQUIRE( b->epath == 0 );
  REQUIRE( b->seqnr == 0 );
}


TEST_CASE( "Subchunk Trailer Size", "[format]" )
{
  REQUIRE( (sizeof(subchunk_trailer) == SUBCHUNK_TRAILER_SIZE ||
            sizeof(subchunk_trailer) == SUBCHUNK_TRAILER_SIZE_32B) );
}


TEST_CASE( "Subchunk Trailer Conversion", "[format]")
{
  const subchunk_trailer* s = subchunk_trailer_from_bytes("\x00\x00");
  REQUIRE( s->type == 0 );
  REQUIRE( s->trunc == 0 );
  REQUIRE( s->err == 0 );
  REQUIRE( s->length == 0 );

  s = subchunk_trailer_from_bytes("\x01\x00");
  REQUIRE( s->length == 1 );

  s = subchunk_trailer_from_bytes("\xFF\x03");
  REQUIRE( s->length == 1023 );
}


TEST_CASE( "Add Subchunks", "[format]")
{
  chunk c;
  const block* b = block_from_bytes(empty_block);

  std::string s1 = "hello";
  std::string s2 = "world";
  subchunk sc1(subchunk::FIRST, s1.c_str(), s1.length(), b);
  subchunk sc2(subchunk::MIDDLE, s2.c_str(), s2.length(), b);

  c.add_subchunk(sc1);
  c.add_subchunk(sc2);

  REQUIRE( c.length() == 10 );
  REQUIRE( c.state() == chunk::PART );

  std::string s3 = "foo";
  c.add_subchunk(subchunk(subchunk::LAST, s3.c_str(), s3.length(), b));
  REQUIRE( c.state() == chunk::FULL );

  c.add_subchunk(subchunk(subchunk::NULL_, s3.c_str(), s3.length(), b));
  REQUIRE( c.state() == chunk::FULL );

  c.add_subchunk(subchunk(subchunk::LAST, s3.c_str(), s3.length(), b));
  REQUIRE( c.state() == chunk::ERROR );
}


TEST_CASE( "Chunk Data", "[format]")
{
  chunk c;
  const block* b = block_from_bytes(empty_block);

  std::string s1 = "hello";
  std::string s2 = "world";
  std::string s3 = "foo";

  c.add_subchunk(subchunk(subchunk::FIRST, s1.c_str(), s1.length(), b));
  c.add_subchunk(subchunk(subchunk::MIDDLE, s2.c_str(), s2.length(), b));
  c.add_subchunk(subchunk(subchunk::LAST, s3.c_str(), s3.length(), b));

  REQUIRE ( c.length() == 13 );
  REQUIRE ( std::strncmp(c.data(), "helloworldfoo", c.length()) == 0 );
}
