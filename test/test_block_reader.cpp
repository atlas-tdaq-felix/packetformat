#include <vector>
#include <memory>

#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include <catch2/catch_test_macros.hpp>

#include "packetformat/block_reader.hpp"


int test_file_fd(void* buf, unsigned size)
{
  int p[2];
  pipe(p);

  if( !fork() )
    {
      for(unsigned len=0; size>len; )
        len += write(p[1], (char*)buf+len, size-len);
      exit(0);
    }

  return p[0];
}


TEST_CASE( "Read test buffer", "[reader]" )
{
  std::vector<felix::packetformat::block> ref_blocks;
  felix::packetformat::block b;
  b.elink = 1;
  b.seqnr = 2;
  b.sob = 3;
  ref_blocks.push_back(b);
  ref_blocks.push_back(b);

  std::vector<int> fds;
  fds.push_back(test_file_fd((void*)ref_blocks.data(),
                             ref_blocks.size()*felix::packetformat::BLOCKSIZE));

  felix::packetformat::BlockReader reader(fds);

  std::vector<std::shared_ptr<felix::packetformat::block>> blocks;
  int nblocks = reader.read_blocks(blocks);

  REQUIRE ( nblocks == 1 );
  REQUIRE ( ref_blocks[0].elink == blocks[0]->elink );

  nblocks = reader.read_blocks(blocks);
  REQUIRE ( nblocks == 1 );
  REQUIRE ( ref_blocks[1].elink == blocks[0]->elink );

}
