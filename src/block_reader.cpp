#include "packetformat/block_reader.hpp"

#include <iostream>
#include <string.h>
#include <cassert>
#include <unistd.h>
#include <sys/epoll.h>


felix::packetformat::BlockReader::BlockReader(const std::vector<int>& file_descriptors,
                                              int maxevents, int timeout, unsigned bsize)
{
  this->file_descriptors = file_descriptors;
  this->maxevents = maxevents;
  this->timeout = timeout;
  this->blocksize = bsize;
  this->events = new struct epoll_event[maxevents];
  setup_epoll(file_descriptors);
}

felix::packetformat::BlockReader::~BlockReader()
{
  delete[] this->events;
}


int
felix::packetformat::BlockReader::read_blocks(
  std::vector<std::shared_ptr<felix::packetformat::block>>& blocks)
{
  int nevents = epoll_wait(epoll_handle, this->events, maxevents, timeout);

  if(nevents == -1)
    {
      // Todo: better error handling
      std::cerr << strerror(errno) << std::endl;
      return 0;
    }

  blocks.resize(nevents);

  for(int i=0; i<nevents; i++)
    {
      blocks[i] = std::make_shared<felix::packetformat::block>();
      read_block_from_fd(events[i].data.fd, blocks[i].get());
    }

  return nevents;
}

void
felix::packetformat::BlockReader::setup_epoll(const std::vector<int>& file_descriptors)
{
  struct epoll_event event;

  this->epoll_handle = epoll_create(file_descriptors.size());
  for(unsigned i=0; i<file_descriptors.size(); i++)
    {
      int fd = file_descriptors[i];
      event.events = EPOLLIN;
      event.data.fd = fd;
      int result = epoll_ctl(epoll_handle, EPOLL_CTL_ADD, fd, &event);
      if(result == -1)
        {
          std::cerr << strerror(errno) << std::endl;
        }
      assert(result == 0);
    }
}

void
felix::packetformat::BlockReader::read_block_from_fd(int fd, felix::packetformat::block* block)
{
  unsigned bytes_read = 0;
  while(bytes_read < blocksize)
    {
      // Todo: could for any reason 0 bytes be read? That would indicate EOF
      bytes_read += read(fd, ((char*)block)+bytes_read, blocksize-bytes_read);
    }
}
