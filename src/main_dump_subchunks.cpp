#include <iostream>
#include <iomanip>
#include <fstream>

#include "packetformat/block_format.hpp"
#include "packetformat/block_parser.hpp"

using namespace std;
using namespace felix::packetformat;


struct SubchunkDump : ParserOperations
{
  unsigned subchunks = 0;
  unsigned subchunks_with_error = 0;
  unsigned chunks = 0;
  unsigned chunks_with_error = 0;
  unsigned blocks = 0;
  unsigned blocks_with_error = 0;

  void subchunk_processed(const subchunk& sc)
  {
    cout << "subchunk> type=" << sc.type << " length=" << sc.length << std::endl;
    subchunks++;
  }
  void subchunk_processed_with_error(const subchunk& sc)
  {
    cout << "subchunk error> type=" << sc.type << " length=" << sc.length << std::endl;
    subchunks_with_error++;
  }
  void chunk_processed(chunk& c)
  {
    chunks++;

    chunk c2 = c;

    unsigned long long test = 0;
    c2.set_subchunk(0, (const char*)&test, sizeof(test));


    cout << "chunk> length=" << std::dec << c2.length();
    char* data = c2.data();

    for(unsigned i=0; i<c2.length(); i++) {
      if (i%32 == 0)
        std::cout << std::endl;
      std::cout << std::hex << std::setw(2) << std::setfill('0') << (data[i] & 0xFF) << " ";
    }
    std::cout << std::endl;
    delete[] data;
  }
  void chunk_processed_with_error(const chunk&)
  {
    chunks_with_error++;
    std::cout << "chunk with error" << std::endl;
  }
  void shortchunk_processed(shortchunk& c)
  {
    chunks++;
    chunks++;
    cout << "shortchunk> length=" << std::dec << c.length;

    for(unsigned i=0; i<c.length; i++) {
      if (i%32 == 0)
        std::cout << std::endl;
      std::cout << std::hex << std::setw(2) << std::setfill('0') << (c.data[i] & 0xFF) << " ";
    }
    std::cout << std::endl;
  }
  void block_processed(const block&)
  {
    blocks++;
  }
  void block_processed_with_error(const block&)
  {
    blocks_with_error++;
    std::cout << "block with error" << std::endl;
  }
};

int
main(int argc, char** argv)
{
  typedef BlockParser<SubchunkDump> block_parser;
  SubchunkDump ops;
  block_parser parser(ops);

  const int max_blocks = 100;
  char bytes[max_blocks*1024];


  FILE* f = fopen(argv[1], "r");
  int n = fread(bytes, 1024, max_blocks, f);
  fclose(f);

  for(int count=0; count < 2; count++)
  {
  for(int i=0; i<n; i++)
    {
      const block* b = block_from_bytes(bytes + i*1024);

      std::cout << "block> sob=" << b->sob << " elink=" << b->elink
                << " seqnr=" << b->seqnr << std::endl;
      //printf("block> 1018,1019=0x%x 0x%x\n", b->data[1018], b->data[1019]);
      //printf("bytes> 1022,1023=0x%x 0x%x\n", bytes[1022], bytes[1023]);

      parser.process(b);
    }
    std::cout << "--iter completed-----------------------" << std::endl;
  }
}
