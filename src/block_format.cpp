#include "packetformat/block_format.hpp"

#include <cstring>
#include <numeric>
#include <cassert>

using namespace felix::packetformat;

static chunk::State	chunk_state_transition_empty(subchunk::Type type);
static chunk::State	chunk_state_transition_part(subchunk::Type type);
static chunk::State	chunk_state_transition(chunk::State current_state,
                                           subchunk::Type type);



const felix::packetformat::block*
felix::packetformat::block_from_bytes(const char* bytes)
{
  return reinterpret_cast<const block*>(bytes);
}


const felix::packetformat::subchunk_trailer*
felix::packetformat::subchunk_trailer_from_bytes(const char* bytes)
{
  return reinterpret_cast<const subchunk_trailer*>(bytes);
}


felix::packetformat::subchunk::subchunk(
  Type t, const char* data, unsigned len, const block* b,
  bool crcerr, bool trunc, bool err )
{
  this->type = t;
  this->data = data;
  this->length = len;
  this->containing_block = b;
  this->crcerr_flag = crcerr;
  this->trunc_flag = trunc;
  this->err_flag = err;
}



static felix::packetformat::chunk::State
chunk_state_transition_empty(felix::packetformat::subchunk::Type type)
{
  using namespace felix::packetformat;

  switch(type)
    {
    case subchunk::FIRST:
      return chunk::PART;
    case subchunk::BOTH:
      return chunk::FULL;
    default:
      return chunk::ERROR;
    }
}

static felix::packetformat::chunk::State
chunk_state_transition_part(felix::packetformat::subchunk::Type type)
{
  using namespace felix::packetformat;

  switch(type)
    {
    case subchunk::LAST:
      return chunk::FULL;
    case subchunk::MIDDLE:
      return chunk::PART;
    default:
      return chunk::ERROR;
    }
}

static felix::packetformat::chunk::State
chunk_state_transition(felix::packetformat::chunk::State current_state,
                       felix::packetformat::subchunk::Type type)
{
  using namespace felix::packetformat;

  switch(current_state)
    {
    case chunk::EMPTY:
      return chunk_state_transition_empty(type);
    case chunk::PART:
      return chunk_state_transition_part(type);
    case chunk::FULL:
    case chunk::ERROR:
    default:
      return chunk::ERROR;
    }
}

void
felix::packetformat::chunk::add_subchunk(const subchunk& sc)
{
  if (sc.type == subchunk::NULL_ ||
      (sc.type == subchunk::TIMEOUT && sc.trunc_flag == false))
    // Skip 'null' and pure TIMEOUT chunks (but not timed-out data chunks)
    return;

  // If we have more subchunks than fit into the array, use std::vector
  if(n_subchunks >= MAX_SUBCHUNKS_IN_ARRAY && subchunk_data_vector.size() == 0)
    {
      subchunk_data_vector.resize(MAX_SUBCHUNKS_IN_ARRAY);
      subchunk_length_vector.resize(MAX_SUBCHUNKS_IN_ARRAY);
      for(unsigned i=0; i<MAX_SUBCHUNKS_IN_ARRAY; i++)
        {
          subchunk_data_vector[i] = subchunk_data[i];
          subchunk_length_vector[i] = subchunk_length[i];
        }
    }

  if(sc.error)
    state_ = ERROR;
  else
    state_ = chunk_state_transition(state_, sc.type);

  // If a vector was created, use that (it means we have more subchunks)
  // than fit into the static array)
  if(subchunk_data_vector.size() > 0)
    {
      subchunk_data_vector.push_back(sc.data);
      subchunk_length_vector.push_back(sc.length);
    }
  else
    {
      this->subchunk_data[n_subchunks] = sc.data;
      this->subchunk_length[n_subchunks] = sc.length;
    }

  n_subchunks++;
}


void
felix::packetformat::chunk::set_subchunk(unsigned int i, const char* data, size_t len)
{
  if (subchunk_data_vector.size() > 0)
  {
    subchunk_data_vector[i] = data;
    subchunk_length_vector[i] = len;
  }
  else
  {
    subchunk_data[i] = data;
    subchunk_length[i] = len;
  }
}

void
felix::packetformat::chunk::invalidate()
{
  this->state_ = chunk::ERROR;
}

const char* const*
felix::packetformat::chunk::subchunks() const
{
  if(subchunk_data_vector.size() > 0)
    {
      return subchunk_data_vector.data();
    }
  else
    {
      return this->subchunk_data;
    }
}


const size_t*
felix::packetformat::chunk::subchunk_lengths() const
{
  if(subchunk_length_vector.size() > 0)
    {
      return subchunk_length_vector.data();
    }
  else
    {
      return this->subchunk_length;
    }
}


size_t
felix::packetformat::chunk::length() const
{
  size_t total = 0;
  for(unsigned i=0; i<n_subchunks; i++)
    {
      total += this->subchunk_lengths()[i];
    }
  return total;
}



char*
felix::packetformat::chunk::data() const
{
  char* d = new char[this->length()];
  unsigned pos = 0;

  for(unsigned i = 0; i < n_subchunks; i++)
    {
      std::memcpy(d+pos, this->subchunks()[i], this->subchunk_lengths()[i]);
      pos += subchunk_length[i];
    }

  return d;
}
