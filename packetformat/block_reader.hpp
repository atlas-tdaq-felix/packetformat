#ifndef FELIX_BLOCK_READER
#define FELIX_BLOCK_READER

#include <vector>
#include <memory>

#include <sys/epoll.h>

#include "packetformat/block_format.hpp"

namespace felix
{
namespace packetformat
{
class BlockReader
{
public:
  BlockReader(const std::vector<int>& file_descriptors,
              int maxevents=64, int timeout=1000, unsigned bsize=BLOCKSIZE);
  ~BlockReader();

  int read_blocks(std::vector<std::shared_ptr<felix::packetformat::block>>& data);

private:
  std::vector<int> file_descriptors;
  int epoll_handle;
  int maxevents;
  int timeout;
  unsigned blocksize;
  struct epoll_event* events;

  void setup_epoll(const std::vector<int>& file_descriptors);
  void read_block_from_fd(int fd, felix::packetformat::block* buf);
};
}
}


#endif
