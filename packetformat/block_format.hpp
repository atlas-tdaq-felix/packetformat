#ifndef FELIX_BLOCK_FORMAT
#define FELIX_BLOCK_FORMAT

#include <cstddef>
#include <cstdint>
#include <vector>
#include <memory>

namespace felix
{
namespace packetformat
{
/*** constants ***/
const size_t   BLOCKSIZE = 1024;
const size_t   BLOCK_HEADERSIZE = 4;
const size_t   SUBCHUNK_TRAILER_SIZE = 2;
const unsigned START_OF_BLOCK = 0xABCD;
const size_t   SUBCHUNK_TRAILER_SIZE_32B = 4;
const unsigned START_OF_BLOCK_32B = 0xABCE;
// NB: this header contains the blocksize encoded in the zero-bits of C0:
const unsigned START_OF_BLOCK_PHASE2 = 0xC0CE;


/*** data types ***/
#pragma GCC diagnostic ignored "-Wpedantic"
struct block
{
  union { // NOTE: Unions need to be integer sized (4 bytes)
    struct {
      unsigned elink   : 11;
      unsigned seqnr   :  5;
      unsigned sob     : 16;
    };
    struct {
      unsigned gbt     : 5;
      unsigned egroup  : 3;
      unsigned epath   : 3;
      unsigned filler1 : 5;
      unsigned filler2 : 16;
    };
  };
  // Covers just the first KB, could extend further to cover n KB
  char data[1020];
};
#pragma GCC diagnostic pop

struct subchunk
{
  enum Type
  {
    NULL_=0, FIRST, LAST, BOTH, MIDDLE, TIMEOUT, OUTOFBAND=7
  };

  Type           type;
  bool           trunc_flag;
  bool           err_flag;
  bool           crcerr_flag;
  unsigned       length;
  const block*   containing_block;
  const char*    data;

  bool error = false;

  subchunk(Type t, const char* data, unsigned len, const block* b,
           bool crcerr=false, bool trunc=false, bool err=false);
};

#pragma GCC diagnostic ignored "-Wpedantic"
struct subchunk_trailer
{
  union { // NOTE: Unions need to be integer sized (4 bytes)
    struct { // 16-bit trailer
      uint32_t length   : 10;
      uint32_t crcerr   :  1;
      uint32_t err      :  1;
      uint32_t trunc    :  1;
      uint32_t type     :  3;
      uint32_t unused   : 16;
    };
    struct { // 32-bit trailer
      uint32_t length_32b   : 16;
      uint32_t unused_32b   :  9;
      uint32_t busymask_32b :  1;
      uint32_t crcerr_32b   :  1;
      uint32_t err_32b      :  1;
      uint32_t trunc_32b    :  1;
      uint32_t type_32b     :  3;
    };
  };
};
#pragma GCC diagnostic pop

class chunk
{
public:
  enum State
  {
    EMPTY, PART, FULL, ERROR
  };

  void   add_subchunk(const subchunk& sc);
  size_t length() const;
  char*  data() const;
  State  state() const
  {
    return this->state_;
  }
  const char* const* subchunks() const;
  const size_t* subchunk_lengths() const;
  // TODO Maybe also make the variable public for performance reasons?
  inline unsigned subchunk_number() const {
    return n_subchunks;
  }

  void set_subchunk(unsigned int i, const char* data, size_t len);
  void invalidate();

  chunk()
  {
    // We reserve one empty subchunk in the beginning of the chunk.
    // This can be used later to substitute a network packet header or similar
    n_subchunks = 1;
    subchunk_data[0] = "";
    subchunk_length[0] = 0;
  }

private:
  static const size_t MAX_SUBCHUNKS_IN_ARRAY = 16;

  size_t n_subchunks;
  const char* subchunk_data[MAX_SUBCHUNKS_IN_ARRAY];
  size_t subchunk_length[MAX_SUBCHUNKS_IN_ARRAY];

  std::vector<const char*> subchunk_data_vector;
  std::vector<size_t> subchunk_length_vector;

  State state_ = EMPTY;
};

class shortchunk
{
public:
  shortchunk(const char* data, size_t length) : data(data), length(length) {}

  const char* data;
  size_t length;
};


/*** function prototypes ***/
const block*  block_from_bytes(const char* bytes);
const subchunk_trailer* subchunk_trailer_from_bytes(const char* bytes);
}
}

#endif
