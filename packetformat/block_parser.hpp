#ifndef FELIX_BLOCK_PARSER
#define FELIX_BLOCK_PARSER

#include "block_format.hpp"

#include "detail/block_parser.hpp"

namespace felix
{
namespace packetformat
{

struct ParserOperations
{
  void chunk_processed(const chunk&) {}
  void chunk_processed_with_error(const chunk&) {}
  void shortchunk_processed(const shortchunk&) {}
  void subchunk_processed(const subchunk&) {}
  void subchunk_processed_with_error(const subchunk&) {}
  void block_processed(const block&) {}
  void block_processed_with_error(const block&) {}
};


template <class OperationsT>
class BlockParser
{
  OperationsT& op;
  detail::BlockParserDetail<OperationsT> impl;

public:
  typedef OperationsT Operations;

  BlockParser(OperationsT& op) : op(op) {}

  void configure( unsigned bsize, bool trailer_is_32bit )
  {
    impl.configure( bsize, trailer_is_32bit );
  }

  void process(const block* block)
  {
    impl.process(block, op);
  }

  void process(const block* blocks, int n)
  {
    for(int i = 0; i < n; i++)
      process(blocks + i);
  }
};
}
}


#endif
