#ifndef FELIX_BLOCK_PARSER_DETAIL
#define FELIX_BLOCK_PARSER_DETAIL

#include <algorithm>
#include <stack>

#include <emmintrin.h>

namespace felix
{
namespace packetformat
{
namespace detail
{

typedef std::stack<subchunk, std::vector<subchunk>> subchunk_stack_t;

template<class OperationsT>
struct BlockParserDetail
{
  felix::packetformat::chunk chunk;
  subchunk_stack_t subchunks;

  unsigned blocksize   = BLOCKSIZE;
  unsigned trailersize = 2;
  bool     trailer_32b = false;

  void configure( unsigned bsize, bool trailer_is_32bit )
  {
    blocksize = bsize;
    trailer_32b = trailer_is_32bit;
    if(trailer_32b)
      trailersize = 4;
    else
      trailersize = 2;
  }

  void submit_subchunk(subchunk& sc, OperationsT& ops)
  {
    if(sc.error)
      ops.subchunk_processed_with_error(sc);
    else
      ops.subchunk_processed(sc);
  }

  void submit_chunk(OperationsT& ops)
  {
    if(chunk.state() == chunk::ERROR)
      {
        ops.chunk_processed_with_error(chunk);
        chunk = felix::packetformat::chunk();
      }
    else
      {
        ops.chunk_processed(chunk);
        chunk = felix::packetformat::chunk();
      }
  }

  void submit_shortchunk(shortchunk& c, OperationsT& ops)
  {
    ops.shortchunk_processed(c);
  }


  bool verify_subchunk(const block*, unsigned, unsigned length,
                       subchunk_trailer t)
  {
    if(length > blocksize-BLOCK_HEADERSIZE-trailersize)
      return false;

    if(trailer_32b)
      {
        if(t.crcerr_32b || t.err_32b || t.trunc_32b)
          return false;
      }
    else
      {
        if(t.crcerr || t.err || t.trunc)
          return false;
      }

    return true;
  }

  bool verify_block(const block* b)
  {
    if( !(b->sob == START_OF_BLOCK ||
          b->sob == START_OF_BLOCK_32B ||
          (b->sob & 0xC0FF) == START_OF_BLOCK_PHASE2) )
      return false;

    return true;
  }

  void read_subchunks(const block* b, subchunk_stack_t& subchunks,
                      OperationsT& ops, bool& errors_occured)
  {
    unsigned pos = blocksize-BLOCK_HEADERSIZE;

    while(pos > 0)
      {
        __builtin_prefetch (b->data+pos-1*64, 0, 0);
        __builtin_prefetch (b->data+pos-2*64, 0, 0);
        __builtin_prefetch (b->data+pos-3*64, 0, 0);
        __builtin_prefetch (b->data+pos-4*64, 0, 0);
        __builtin_prefetch (b->data+pos-5*64, 0, 0);
        __builtin_prefetch (b->data+pos-6*64, 0, 0);
        __builtin_prefetch (b->data+pos-7*64, 0, 0);
        __builtin_prefetch (b->data+pos-8*64, 0, 0);

        const subchunk_trailer *t =
          subchunk_trailer_from_bytes(b->data+pos-trailersize);

        // Subchunk length
        unsigned sc_len;
        if(trailer_32b)
          sc_len = t->length_32b;
        else
          sc_len = t->length;

        // Jump to the start of this chunk
        pos -= sc_len + trailersize;

        // Account for possible padding bytes
        if( (sc_len & (trailersize-1)) != 0 )
          pos -= (trailersize - (sc_len & (trailersize-1)));
        //std::cout << "len=" << sc_len << " pos=" << pos << " t=" << t->type << std::endl;

        if(pos > blocksize-BLOCK_HEADERSIZE)
          {
            pos = 0;
            sc_len = 0;
            errors_occured = true;
            continue;
          }

        if(trailer_32b)
          subchunks.emplace((subchunk::Type)t->type_32b, b->data+pos,
                            sc_len, b, (bool)t->crcerr_32b, (bool)t->trunc_32b, (bool)t->err_32b);
        else
          subchunks.emplace((subchunk::Type)t->type, b->data+pos,
                            sc_len, b, (bool)t->crcerr, (bool)t->trunc, (bool)t->err);

        if(verify_subchunk(b, pos, sc_len, *t) == false)
          {
            subchunks.top().error = true;
            errors_occured = true;
          }

        submit_subchunk(subchunks.top(), ops);
      }
  }

  void add_subchunks_to_chunk(subchunk_stack_t& subchunks,
                              OperationsT& ops,
                              bool& errors_occured)
  {
    while(!subchunks.empty())
      {
        subchunk sc = subchunks.top();
        subchunks.pop();

        // In case the chunk is in error state, and the next
        // subchunk can start a new chunk, create new chunk
        if(chunk.state() == chunk::ERROR &&
            (sc.type == subchunk::FIRST || sc.type == subchunk::BOTH))
          {
            submit_chunk(ops);
            errors_occured = true;
          }

        if(sc.type == subchunk::BOTH)
          {
            if(chunk.state() != chunk::EMPTY)
              {
                chunk.invalidate();
                submit_chunk(ops);
              }
            shortchunk s(sc.data, sc.length);
            submit_shortchunk(s, ops);
          }
        else
          {
            chunk.add_subchunk(sc);
          }

        if(chunk.state() == chunk::FULL)
          {
            submit_chunk(ops);
          }
      }

    // After each block we check if the current chunk
    // is in error state, and report it
    if(chunk.state() == chunk::ERROR)
      {
        submit_chunk(ops);
        errors_occured = true;
      }
  }


  void process(const block* b, OperationsT& ops)
  {
    if(!verify_block(b))
      {
        ops.block_processed_with_error(*b);
      }
    else
      {
        bool errors_occured = false;

        read_subchunks(b, subchunks, ops, errors_occured);
        add_subchunks_to_chunk(subchunks, ops, errors_occured);

        if(errors_occured)
          {
            ops.block_processed_with_error(*b);
          }
        else
          {
            ops.block_processed(*b);
          }
      }
  }
};
}
}
}

#endif
